package com.taledir.microservices;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailController {

  Logger logger = LoggerFactory.getLogger(EmailController.class);

	@Autowired
	EmailService emailService;

  @PostMapping("/email/sendPasswordReset")
  public ResponseEntity<Object> sendPasswordReset(@Valid @RequestBody PasswordResetDto passwordResetDto,
      BindingResult bindingResult) {

    if (bindingResult.hasErrors()) {
      if (bindingResult.getFieldError() != null) {
        throw new EmailBadRequestException(bindingResult.getFieldError().getDefaultMessage());
      }
    }

    String subject = "Password Reset Request Code";

    String header = "Your Password Reset Code";
    String message = "Use the following code to reset your password:  ";

    String htmlMessage = "<h1>" + header + "</h1><br/>" +
      "<p>" + message + "<strong>" + passwordResetDto.getToken() + "</strong></p>";

    emailService.sendEmail(passwordResetDto.getEmail(), subject, htmlMessage);

    Map<String, Object> body = new HashMap<String, Object>();
    body.put("success", true);

    return ResponseEntity.ok().body(body);
  }

}
