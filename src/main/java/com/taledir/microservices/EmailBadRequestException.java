package com.taledir.microservices;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EmailBadRequestException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EmailBadRequestException(String arg0) {
		super(arg0);
	}

}

